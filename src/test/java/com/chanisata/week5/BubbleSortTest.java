package com.chanisata.week5;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BubbleSortTest {

    @Test
    public void shouldBubbleTest1() {
        int arr[] = { 5, 4, 3, 2, 1 };
        int expected[] = { 4, 3, 2, 1, 5 };
        int first = 0;
        int second = 4;
        BubbleSort.bubble(arr, first, second);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void shouldBubbleTest2() {
        int arr[] = { 4, 3, 2, 1, 5 };
        int expected[] = { 3, 2, 1, 4, 5 };
        int first = 0;
        int second = 3;
        BubbleSort.bubble(arr, first, second);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void shouldBubbleTest3() {
        int arr[] = { 3, 2, 1, 4, 5 };
        int expected[] = { 2, 1, 3, 4, 5 };
        int first = 0;
        int second = 2;
        BubbleSort.bubble(arr, first, second);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void shouldBubbleTest4() {
        int arr[] = { 2, 1, 3, 4, 5 };
        int expected[] = { 1, 2, 3, 4, 5 };
        int first = 0;
        int second = 1;
        BubbleSort.bubble(arr, first, second);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void shouldBubbleSortTest1() {
        int arr[] = { 5, 4, 3, 2, 1 };
        int expected[] = { 1, 2, 3, 4, 5 };
        BubbleSort.bubbleSort(arr);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void shouldBubbleSortTest2() {
        int arr[] = { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
        int sorted[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        BubbleSort.bubbleSort(arr);
        assertArrayEquals(sorted, arr);
    }

    @Test
    public void shouldBubbleSortTest3() {
        int arr[] = { 6, 9, 3, 7, 10, 5, 4, 8, 2, 1 };
        int sorted[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        BubbleSort.bubbleSort(arr);
        assertArrayEquals(sorted, arr);
    }
}
