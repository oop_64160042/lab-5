package com.chanisata.week5;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SelectionSortAppTest {
    //Min
    //หาตำแหน่งค่าน้อยที่สุด ตั้งแต่ตำแหน่งที่0
    @Test
    public void  shouldFindMinIndexTest1(){
        int arr[] = {5,4,3,2,1};
        int pos = 0;
        int minIndex = SelectionSortApp.FindMinIndex(arr,pos);
        assertEquals(4, minIndex);
    }

    //หาตำแหน่งค่าน้อยที่สุด ตั้งแต่ตำแหน่งที่1
    @Test
    public void  shouldFindMinIndexTest2(){
        int arr[] = {1,4,3,2,5};
        int pos = 1;
        int minIndex = SelectionSortApp.FindMinIndex(arr,pos);
        assertEquals(3, minIndex);
    }

    //หาตำแหน่งค่าน้อยที่สุด ตั้งแต่ตำแหน่งที่2
    @Test
    public void  shouldFindMinIndexTest3(){
        int arr[] = {1,2,3,4,5};
        int pos = 2;
        int minIndex = SelectionSortApp.FindMinIndex(arr,pos);
        assertEquals(2, minIndex);
    }

    @Test
    public void  shouldFindMinIndexTest4(){
        int arr[] = {1,1,1,1,1,0,0,1,1};
        int pos = 0;
        int minIndex = SelectionSortApp.FindMinIndex(arr,pos);
        assertEquals(5, minIndex);
    }


    //Swap
    @Test
    public void  shouldSwapTest1(){
        int arr[] = {5,4,3,2,1};
        int expectedArray[] = {1,4,3,2,5};
        int first = 0;
        int second = 4;
        SelectionSortApp.swap(arr,first,second);
        assertArrayEquals(expectedArray , arr);
    }

    @Test
    public void  shouldSwapTest2(){
        int arr[] = {5,4,3,2,1};
        int expectedArray[] = {5,4,3,2,1};
        int first = 0;
        int second = 0;
        SelectionSortApp.swap(arr,first,second);
        assertArrayEquals(expectedArray , arr);
    }

    @Test
    public void  shouldSelectionSortTest1(){
        int arr[] = {5,4,3,2,1};
        int sortedArray[] = {1,2,3,4,5};
        SelectionSortApp.SelectionSort(arr);
        assertArrayEquals(sortedArray, arr);
    }

    @Test
    public void  shouldSelectionSortTest2(){
        int arr[] = {10,9,8,7,6,5,4,3,2,1};
        int sortedArray[] = {1,2,3,4,5,6,7,8,9,10};
        SelectionSortApp.SelectionSort(arr);
        assertArrayEquals(sortedArray, arr);
    }

    @Test
    public void  shouldSelectionSortTest3(){
        int arr[] = {6,9,3,7,10,5,4,8,2,1};
        int sortedArray[] = {1,2,3,4,5,6,7,8,9,10};
        SelectionSortApp.SelectionSort(arr);
        assertArrayEquals(sortedArray, arr);
    }
}
